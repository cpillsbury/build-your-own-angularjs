/* jshint globalstrict: true */
'use strict';


// Used to initialize the "last" value for a given watcher object (See $watch watcher, below).
// This ensures that undefined return values for the watch function will still show up as dirty
// on initialization.
function initWatchVal() {}

// Our implementation of AngularJS Scopes.
function Scope() {

	// Initialize array that holds objects containing a watcher function/listener function pair.
	//NOTE: "$$" prefixed variables are framework internals and should not be called from application code.
	this.$$watchers = [];

	// Used to store the last dirty watch found in a digest cycle. Allows for short-circuiting iteration
	// over watchers when watchers earlier in the watcher list were the only dirty ones. Initialize to null.
	//NOTE: "$$" prefixed variables are framework internals and should not be called from application code.
	this.$$lastDirtyWatch = null;

	// Used to store functions that should be evaluated asynchronously at the end of a digest cycle.
	// These are removed as they're executed. 
	// See: $evalAsync(), $digest()
	this.$$asyncQueue = [];

	// Used to store functions that should be called after the next digest cycle.
	// Similar to $$asyncQueue, except:
	// a) Doesn't automatically schedule a deferred digest cycle -AND-
	// b) Executes functions *after* a digest cycle, instead of *at the end of* but still *durring* a digest cycle.
	this.$$postDigestQueue = [];

	// Used to store a reference to the root scope. Used by descendant scopes created with prototypal inheritance 
	// using the $new() method to get access to the top-most scope in the scope tree (See: $apply(), $evalAsync()).
	this.$$root = this;

	// Used to store all and only children of the current scope, which are created via prototypal inheritance using the
	// Scope.prototype.$new() method (See $new()).
	this.$$children = [];

	// Used to store the current phase of the digest cycle. Set using $beginPhase() and clear using
	// $clearPhase(). Should be cleared between phases.
	this.$$phase = null;
}

// $new function should be added to the Scope prototype. This allows for prototypal inheritance.
// $new() is used to create child scopes from a parent scope, allowing them to inherit all properties
// assigned to the parent. The optional "isolated" parameter is used to create an isolate scope,
// or one that will not be created using prototypal inheritance and therefore will not have reference
// to its anscestor's properties.
Scope.prototype.$new = function(isolated) {
	var child;

	if(isolated) {
		// If the child should be an isolate scope, simply create a new Scope() object.
		child = new Scope();
		// Since isolate scopes don't rely on prototypal inheritance, we have to set
		// these properties explicitly to ensure that methods that *should* refer to ancestors
		// still work as expected (See: $apply() & $evalAsync()).
		child.$$root = this.$$root;
		child.$$asyncQueue = this.$$asyncQueue;
		child.$$postDigestQueue = this.$$postDigestQueue;
	} else {
		// Otherwise, set up the prototypal inheritance and apply it to the child.

		// Define a ChildScope object to be used to create new child scopes from a parent scope.
		var ChildScope = function() { };
		// Assign the current scope as the prototype of the ChildScope. This is what allows
		// the current scope to act as the "parent" of a child scope by taking advantage of
		// JS prototypal inheritance functionality.
		// For more on JS prototypes, see: http://dailyjs.com/2012/05/21/js101-prototype/
		ChildScope.prototype = this;
		// Now that we've assigned this scope as the prototype of the ChildScope, create
		// a new instance of ChildScope(). This will be the child scope.
		child = new ChildScope();
	}

	// Add the newly created child scope instance to the current scope's collection of children.
	this.$$children.push(child);

	// Create a *new* $$children property on the child scope. This will shadow the parent scope's $$children,
	// thereby ensuring that: (a) none of the parent scope's children will show up in the newly created child scope
	// collection of $$children & (b) children added to the newly created child scope will *not* update the parent
	// scope's collection of children.
	child.$$children = [];

	// Create a *new* $$watchers property on the child scope. This will shadow the parent scope's
	// $$watchers property, thereby ensuring that watchers added to the parent scope won't get called
	// when digesting a child scope's watchers.
	child.$$watchers = [];

	// Store a reference to the parent on the created child.
	child.$parent = this;

	return child;
};

// $destroy function should be added to the Scope prototype. This allows for prototypal inheritance.
// $destroy() is used to remove the current scope from the collection of children stored on its parent,
// removing it from ancestor digest cycles, allowing it to be garbage collected, etc.
Scope.prototype.$destroy = function() { 
	// If this is the root scope, no need to destroy it, since it has no parent references.
	if (this === this.$$root) {
		return;
	}
	
	// Get the parent scope's collection of children.
	var siblings = this.$parent.$$children;
	// Find the current scope in this collection.
	var indexOfThis = siblings.indexOf(this);

	// If it's actually in the collection of children...
	if (indexOfThis >= 0) {
		// Remove it from the list.
		siblings.splice(indexOfThis, 1);
	}
};

// $$everyScope function should be added to the Scope prototype. This allows for prototypal inheritance.
// $$everyScope is a recursive function that executes the function "fn" (passed in as a parameter) on the
// current scope and all descendant scopes that have "fn".
Scope.prototype.$$everyScope = function(fn) { 
	// Attempt to execute fn on the current scope, passing itself in as a parameter. This means "fn" should
	// have a signature that takes a scope as a parameter.
	// In addition, this is used to conditionally check if it should execute the same function on its children.
	// This means "fn" should also have a signature that returns a "truthy" value.
	// This is used in conjunction with the digest cycle, which returns "true" if the digest is dirty (See: $$digestOnce()).
	//
	// NOTE: Since descendant scopes should inherit its anscestor's functions via prototypal inheritance, it's
	// safe to assume that, if a function exists on a parent, it also exists on all descendants, too, unless it's
	// been explicitly overridden (shadowed). However, we *can* break the recursive chain if, for example, 
	// a descendant sets the function to null but *its* descendant re-assigns a function with a valid signature to the property.
	if (fn(this)) {
		// If we've executed the function on the current scope, recursively execute it on all of its children (each of which will
		// in turn execute it on *their* children, etc.)
		return this.$$children.every(function(child) { 
			return child.$$everyScope(fn);
		});
	// If the function returns false, we can break out of the recursion and notify the caller that the condition wasn't met.
	// This is used in conjunction with the digest cycle (See: $$digestOnce()).
	} else {
		return false;
	}
};

// $watch function should be added to the Scope prototype. This allows for prototypal inheritance.
Scope.prototype.$watch = function(watchFn, listenerFn, valueEq) {
	// Use the self var as a reference to "this" in the context of the Scope object.
	// NOTE: Using "self" gets around peculiar "this" binding behaviors in JavaScript.
	// See: http://alistapart.com/article/getoutbindingsituations for more on the problem and the pattern.
	var self = this;
	// watcher objects have a watch function, a listener function, and a last property which
	// tracks the last value returned from the watch function, which is used for dirty checking.
	var watcher = {
		watchFn: watchFn,
		// If no listener function, create an empty dummy listener function.
		// This allows us to add watch functions to the list of watchers
		// without a corresponding listener function. One use case would be
		// to get notified every time $digest is called.
		listenerFn: listenerFn || function() {},
		// Indicates whether or not we should dirty check by value. Default is no (check be reference
		// instead).
		// NOTE: Using "!!" ensures that if *no* valueEq was passed in as a param, the valueEq
		// property on the watcher will equal false.
		valueEq: !!valueEq,
		last: initWatchVal
	};

	// Add the watcher object to the array of watchers.
	// NOTE: Instead of pushing, add new watchers to the beginning of the watchers array by using unshift().
	// This is done to handle watcher removal while in a digest cycle, which iterates over watchers
	// last to first. That way, if one is removed, it won't change the indices of watchers later in the
	// digest cycle's iteration. (See: $$digestOnce())
	this.$$watchers.unshift(watcher);

	// Reset $$lastDirtyWatch to null whenever we add a new watcher. This ensures
	// that the digest will check if these new watchers are dirty even if they are
	// added in the middle of a digest cycle (Ex: if they are added in the listener function
	// of another watcher).
	// Track the $$lastDirtyWatch on the root, since digest cycles initiated by calling $apply() or $evalAsync()
	// on the *current scope* will actually begin at this scope's root scope, recursing over all of its descendants,
	// including this one (See: $apply() & $evalAsync()).
	this.$$root.$$lastDirtyWatch = null;

	// Return a function that, if called, will remove the watcher just added to this scope.
	return function() {
		// Get the index of the current watcher just added to $$watchers.
		var index = self.$$watchers.indexOf(watcher);
		// Verify that it is still an element on $$watchers by the time the function is called.
		if(index>=0) {
			// If it is, remove it from the $$watchers list. Since this is used by all digest
			// cycle functionality, this means the watcher/listener pair will not be a 
			// part of subsequent digest cycles.
			self.$$watchers.splice(index,1);
			// Removing the watcher will change the indices of other watchers which will break
			// the lastDirtyWatch optimiziation functionality in the digest cycle. Therefore, 
			// clear $$lastDirtyWatch whenever a watcher is removed. (See: $$digestOnce())
			self.$$root.$$lastDirtyWatch = null;
		}
	};
};

// $$areEqual function should be added to the Scope prototype. This allows for prototypal inheritance.
// $$areEqual is used to dirty check watchers by value change or reference change, depending on what
// they were configured to do when created by the $watch() method.
Scope.prototype.$$areEqual = function(newValue, oldValue, valueEq) {
	if (valueEq) {
		// If dirty checking by value, use the lo-dash isEqual() function.
		return _.isEqual(newValue, oldValue);
	} else {
		return newValue === oldValue ||
		// Need to handle NaN condition, since NaN != NaN in JavaScript.
		(typeof newValue === 'number' && typeof oldValue === 'number' && isNaN(newValue) && isNaN(oldValue));
	}
};

// $$digestOnce function should be added to the Scope prototype. This allows for prototypal inheritance.
// $$digestOnce does a single iteration over all registered watchers to check if any return changes.
// If so, returns dirty to notify $digest that it should re-call $$digestOnce().
//NOTE: "$$" prefixed variables are framework internals and should not be called from application code.
Scope.prototype.$$digestOnce = function() {
	
	var dirty;
	var continueLoop = true;

	// Use the self var as a reference to "this" in the context of the Scope object.
	// NOTE: Using "self" gets around peculiar "this" binding behaviors in JavaScript.
	// See: http://alistapart.com/article/getoutbindingsituations for more on the problem and the pattern.
	var self = this;

	// Use $$everyScope() to recursively execute the following code on descendants of the current scope.
	// NOTE: Every place where "self" is used will refer to the initiating anscestor scope, whereas every
	// place where "scope" is used will refer to the particular descendant scope being checked in the recursive
	// function call. Also note that "dirty" and "continueLoop" are declared *outside* of this function call. This
	// means that every reference to "dirty" or "continueLoop" will refer to *the same* variable on the initiating ancestor
	// scope's $$digestOnce() function, regardless of the level of recursion.
	this.$$everyScope(function(scope) {

		// Declare newValue, oldValue, and dirty vars once outside of the forEach method. dirty is used
		// to notify the calling function that a value has been changed and the digest should be re-run.
		//
		// NOTE: THE BOOK DOESN'T HIGHLIGHT IN THE CODE SNIPPET (P. 58) THE FACT THAT THIS LINE OF CODE CHANGED FROM:
		// var newValue, oldValue, dirty; 
		// TO:
		// var newValue, oldValue;
		// IF YOUR TESTS FROM THE SECTION "SCOPE INHERITANCE - RECURSIVE DIGESTION", THIS MAY BE WHY. (CJP)
		var newValue, oldValue;

		// Use lo-dash to iterate over each element in the $$watchers array, executing the function defined.
		// Iterate in reverse to handle removing watchers while in a digest cycle, since removal will change
		// the indices of watchers later in the $$watchers collection, but not earlier ones.
		_.forEachRight(scope.$$watchers, function(watcher) {
			// Wrap each iteration in a try-catch block to ensure that errors thrown in watch functions or
			// listener functions do not interrupt the digest cycle as a whole and break central functionality
			// of Angular.
			try {
				// Do a null check on the watcher, since it may have been removed/destoryed while in the middle of
				// the digest cycle.
				if (watcher) {
					// Call the watch function on the current watcher object, passing in a reference to the self scope.
					// The return value of the watch function represents the current value of what's being watched.
					// This is used for dirty checking, below.
					newValue = watcher.watchFn(scope);

					// The oldValue is whatever the watcher object has stored in its last property. See below for
					// when this gets updated. This is used for dirty checking, below.
					// NOTE: The first time $digest is called, watcher.last will be undefined, so will be considered
					// dirty if newValue is set to anything. This is desired behavior for initialization.
					oldValue = watcher.last;

					// Determine if a watcher is dirty by checking if they are equal. This will either be based
					// on reference equality (default) or by value equality (optional param when constructing
					// a watcher using the $watch() function).
					// See: $$areEqual()
					if (!scope.$$areEqual(newValue, oldValue, watcher.valueEq)) {
						// Since this watcher reported dirty, register it as the last dirty watcher. If another
						// watcher later in the watcher list is also dirty, it will replace the current one.
						scope.$$root.$$lastDirtyWatch = watcher;
						// Update the last property on the current watcher object for subsequenty dirty checking.
						// If we are dirty checking against value instead of default dirty check (which just compares
						// references), use lo-dash cloneDeep() function to make a complete clone of the object.
						// If we didn't create a clone of the object and instead assigned it by reference,
						// changes to newValue would also change watcher.last for complex objects, and thus
						// the watcher would never show up as dirty.
						watcher.last = (watcher.valueEq ? _.cloneDeep(newValue) : newValue);

						// Since dirty, call the listener function. Pass newValue, oldValue, and the current scope instance.
						watcher.listenerFn(newValue, 
							// Use ternary operator to see if the old value was strictly equal to our default initWatchVal.
							// This ensures that the oldValue passed to the listener function doesn't misrepresent
							// what the *actual* last value was. If the newValue is *also* an empty JavaScript Object,
							// this value will be passed regardless.
							(oldValue === initWatchVal ? newValue : oldValue), 
							scope);

						// If the values have changed, assume the scope may have been "dirtied" and we should re-run the 
						// the $digest.
						dirty = true;
					} else if (scope.$$root.$$lastDirtyWatch === watcher) {

						continueLoop = false;

						// If we've made it to the last watcher to show up as dirty, we can bail on the digest cycle.
						// To work, this depends on $$lastDirtyWatch being reset to null at the begining of a new digest 
						// cycle (See: $digest()).
						return false;
					}
				}
			} catch (e) {
				// If an error is thrown, print it out to the JS console as an error.
				console.error(e);
			}
		});

		return continueLoop;
	});

	return dirty;
};

// $digest function should be added to the Scope prototype. This allows for prototypal inheritance.
Scope.prototype.$digest = function() {
	// This variable represents the max number of iterations we should keep digesting if scope is
	// potentially dirty. This protects against infinite loops.
	var ttl = 10;
	// This variable represents when the scope is potentially dirty because a watcher value has changed
	// and therefore has called its listener function, which may have updated the scope.
	var dirty;

	// Need to reset $$lastDirtyWatch on the root to null whenever we begin a digest cycle, since we don't yet know
	// what is (or will be) dirty, and since digest cycles can start at the root scope and recurse over its descendants
	// (including the current scope).
	this.$$root.$$lastDirtyWatch = null;

	this.$beginPhase("$digest");

	// Use a do-while loop so we run $$digestOnce at least once for a $digest call.
	do {
		// Iterate over all of the $evalAsync functions stored in the $$asyncQueue and $eval them.
		while (this.$$asyncQueue.length) {
			// Wrap each call to functions in the asyncQueue in a try-catch block to ensure that
			// functions that throw an error won't interrupt the expected digest cycle functionality.
			try {
				// Get the first function stored in the $$asyncQueue and remove it from the queue.
				var asyncTask = this.$$asyncQueue.shift();
				// $eval the function now that we're in the digest cycle.
				asyncTask.scope.$eval(asyncTask.expression);
			} catch (e) {
				// If an error was thrown, print it out to the JS console as an error.
				console.error(e);
			}
		}
		// If one or more watcher values have changed, assume dirty and run through digest again.
		dirty = this.$$digestOnce();

		// If we're still dirty after we've decremented our ttl to 0 (ie looped 10 times), throw an error.
		// This means our digest is potentially unstable since listeners are updating scope properties that
		// themselves have watchers, etc.
		// The same goes for functions added to the asyncQueue that are $eval'ed asynchronously (during the
		// digest cycle). If the asyncQueue is never empty, it is potentially unstable since something
		// keeps adding $evalAsync functions to the asyncQueue in the digest cycle.
		if ((dirty || this.$$asyncQueue.length) && !(ttl--)) {
			this.$clearPhase();
			throw "10 digest iterations reached";
		}
		// Check if *either* the digest is dirty (i.e. a watcher registered a change) *or* there are functions
		// still in the asyncQueue.
	} while (dirty || this.$$asyncQueue.length);
	this.$clearPhase();

	// Once we've finished the digest cycle, if there are any functions in the $$postDigestQueue, execute them.
	while (this.$$postDigestQueue.length) {
		// Wrap each call to functions in the postDigestQueue in a try-catch block to ensure that functions that throw
		// an error won't interrupt the expected (post) digest cycle functionality.
		try {
			// This does two things: 
			// 1) Removes the first function in the $$postDigestQueue from the queue by calling shift() on the queue.
			// 2) Since shift() returns a reference to the element just removed (in this case, the function added to the queue),
			//    the second "()" below means we are executing the function we just removed from the queue.
			this.$$postDigestQueue.shift()();
		} catch (e) {
			// If an error was thrown, print it out to the JS console as an error.
			console.error(e);
		}
	}
};

// $eval function should be added to the Scope prototype. This allows for prototypal inheritance.
Scope.prototype.$eval = function(expr, locals) {
	// Executes a function in the context of Angular, passing the scope and (optionally) an argument
	// and returning the function's return value.
	return expr(this, locals);
};

// $evalAsync function should be added to the Scope prototype. This allows for prototypal inheritance.
Scope.prototype.$evalAsync = function(expr) {
	// Use the self var as a reference to "this" in the context of the Scope object.
	// NOTE: Using "self" gets around peculiar "this" binding behaviors in JavaScript.
	// See: http://alistapart.com/article/getoutbindingsituations for more on the problem and the pattern.
	var self = this;
	// If we're not currently in a digest cycle phase and haven't already added any functions to the 
	// asyncQueue (by calling this function, $evalAsync()), then schedule a deferred $digest().
	// The first condition means we're already in a digest cycle, so there's no need to schedule a new one.
	// The second condition means we've already scheduled a deferred digest cycle, since we add functions
	// to the asyncQueue by calling this function ($evalAsync()), and, they are only removed during a digest cycle.
	if (!self.$$phase && !self.$$asyncQueue.length) {
		// Use a setTimeout() call with a timeout of 0 ms to set up a deferred $digest() call ASAP.
		// NOTE: This doesn't mean $digest() will get called in 0 ms, since timers are usually only precise up to
		// about 50 ms. It does mean $digest() will get called as soon as the system is able to trigger the timeout.
		setTimeout(function() {
			// Check to see if the asyncQueue is still populated. If it isn't, this means $digest was explicitly called
			// (either directly through a call to $digest() or indirectly through a call to $apply()) between the time
			// the timeout was set up and the time the timeout was triggered.
			if (self.$$asyncQueue.length) {
				// The deferred call to the root scope's $digest() to kick off a digest cycle.
				// NOTE: 
				self.$$root.$digest();
			}
		}, 0);
	}
	// Add function to the $$asyncQueue list, to be executed later in during digest cycle.
	// Store scope in queue to keep track of appropriate scope in scope inheritance tree (to be implemented later)
	this.$$asyncQueue.push({scope: this, expression: expr});
};

// $apply function should be added to the Scope prototype. This allows for prototypal inheritance.
Scope.prototype.$apply = function(expr) {
	try {
		this.$beginPhase("$apply");
		// First execute and evaluate the function passed in as a param to $apply().
		// NOTE: Although $apply() kicks off a digest cycle on the root scope (see below), eval can still be executed on
		// the current scope.
		return this.$eval(expr);
	} finally {
		this.$clearPhase();
		// Then kick off a digest cycle on the root, since the function may have dirtied a property being watched on the scope.
		// NOTE: Calling a $digest() on the root scope will digest on *all* descendant scopes, including the current one
		// (See: $$digestOnce()).
		this.$$root.$digest();
	}
};

// $beginPhase function should be added to the Scope prototype. This allows for prototypal inheritance.
// This function sets the current phase of a given digest cycle, including indirect calls to $digest() through $apply().
Scope.prototype.$beginPhase = function(phase) {
	// If we're currently in a phase but try to begin a new phase, throw an error. For proper use, call 
	// $clearPhase() before beginning a new phase via a $beginPhase() call. (See: $clearPhase())
	if (this.$$phase) {
		throw this.$$phase + ' already in progress.';
	}
	// Set the current phase.
	this.$$phase = phase;
};

// $clearPhase function should be added to the Scope prototype. This allows for prototypal inheritance.
// This function clears the current phase of a given digest cycle. Use in between digest phases set via $beginPhase().
// (See: $beginPhase())
Scope.prototype.$clearPhase = function() {
	// Clear the current phase.
	this.$$phase = null;
};

Scope.prototype.$$postDigest = function(fn) {
	// Add the function passed in as a parameter to the $$postDigestQueue, to be executed *after* the next digest cycle.
	this.$$postDigestQueue.push(fn);
};