/* jshint globalstrict: true */
/* global Scope: false */
'use strict';

// Tests for our Scope implementation
describe("Scope", function() {

	// test demonstrating that Scopes can be used as plain old JavaScript Objects (POJOs)
	it("can be constructed and used as an object", function () {
		var scope = new Scope();
		scope.aProperty = 1;

		expect(scope.aProperty).toBe(1);
	});

	// Tests for our digest functionality implementation.
	describe("digest", function() {

		var scope;

		// Execute this before each test.
		beforeEach(function() {
			scope = new Scope();
		});

		// NOTE: THIS IS NOT A TEST FROM THE BOOK
		it("returns true if a watcher's value changes and thus dirty digest", function() {
			scope.someValue = 'Some value';
			
			scope.$watch(
				function(scope) { return scope.someValue; },
				function(newValue, oldValue, scope) { }
			);

			// This means we expect a call to $$digestOnce() to return true, since
			// the watcher associated with someValue should have registered a value change
			// and thus the current scope should be considered dirty.
			expect(scope.$$digestOnce()).toBe(true);

			// This means we expect a second call to $$digestOnce() (without changing)
			// the scope property to not return a value of true, since nothing on the scope
			// is dirty.
			// NOTE: since we only care about the condition where $$digestOnce() returns
			// true, we should not assume it will return false otherwise. In fact,
			// it currently returns undefined. Hence, we need to verify this by only expecting
			// the negative condition of returning true, that is, "not.toBe(true)".
			// See: Scope $$digestOnce().
			expect(scope.$$digestOnce()).not.toBe(true);

			scope.someValue = 'Some other value';
			// This means we expect a call to $$digestOnce() to return true, since we've again
			// changed a scope property (someValue) associated with a watcher's watch function.
			expect(scope.$$digestOnce()).toBe(true);
		});

		it("calls the listener function of a watch on first $digest", function() {
			var watchFn	= function() { return 'wat'; };

			// Use jasmine spy for mock functionality to determine basic function calls.
			var listenerFn = jasmine.createSpy();
			scope.$watch(watchFn, listenerFn);

			scope.$digest();

			// This means we expect the listener function to have been called after calling $digest().
			expect(listenerFn).toHaveBeenCalled();
		});

		it("calls the listener function when watched value changes", function() {			
			// Initial values to test changes against.
			scope.someValue = 'a';
			scope.counter = 0;

			scope.$watch(
				// watch function for the someValue property.
				function(scope) { return scope.someValue; },

				// listener function to be called when someValue property changes.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			// This means we expect the counter prop to be zero, since no digest has occurred.
			expect(scope.counter).toBe(0);

			scope.$digest();
			// This means we expect the counter to have incremented (from the listener function),
			// since $digest was called. The first time $digest is called for a watched property
			// should register as a change, since it's changed from nothing to something.
			expect(scope.counter).toBe(1);

			scope.$digest();
			// This means we expect the counter to have not incremented, since someValue hasn't
			// changed since the last time $digest was called.
			expect(scope.counter).toBe(1);

			// Change someValue.
			scope.someValue = 'b';
			// This means we expect the counter to *still* have not incremented. Although someValue
			// has now changed, we haven't called $digest again, which means the listener function
			// won't have been called yet.
			expect(scope.counter).toBe(1);

			scope.$digest();
			// This means we expect the counter to have incremented (from the listener function),
			// since we've *both* changed someValue *and* called $digest.
			expect(scope.counter).toBe(2);

		});

		it("calls the listener when watch value is first undefined", function () {
			scope.counter = 0;

			scope.$watch(
				// The watch function.
				function(scope) { return scope.someValue; },
				// The listener function.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			scope.$digest();
			// This means we expect the counter to increment (from the listener function),
			// even if value watched (in this case, scope.someValue) or return value from
			// the watch function is undefined.
			expect(scope.counter).toBe(1);
		});

		it("calls listener with new value as old value the first time", function() {
			scope.someValue = 123;
			var oldValueGiven;

			scope.$watch(
				// The watch function.
				function(scope) { return scope.someValue; },
				// The listener value. Updates oldValueGiven to equal whatever
				// was passed as the oldValue to the listener function from
				// the Scope $digest function.
				// See: Scope.prototype.$digest
				function(newValue, oldValue, scope) { oldValueGiven = oldValue; }
			);

			scope.$digest();
			// This means we expect the old value to be whatever value was returned
			// from the watch function on initial $digest call.
			expect(oldValueGiven).toBe(123);
		});

		it("calls the watch function with the scope as the argument", function() {

			// Use jasmine spy for mock functionality to determine basic function calls.
			var watchFn = jasmine.createSpy();
			var listenerFn = function() { };

			scope.$watch(watchFn, listenerFn);

			scope.$digest();

			// This means we expect the watch function to have been called with the scope object as a param.
			expect(watchFn).toHaveBeenCalledWith(scope);
		});

		it("may have watchers that omit the listener function", function () {
			var watchFn = jasmine.createSpy().and.returnValue('something');

			// Add a watch function to the scope's watchers without a corresponding listener funciton.
			scope.$watch(watchFn);

			scope.$digest();

			// This means we still expect the watch function to be called even if there is no
			// corresponding listener function.
			expect(watchFn).toHaveBeenCalled();
		});

		it("triggers chained watchers in the same digest", function() {
			scope.name = 'Jane';

			// Watcher corresponding to nameUpper property on scope.
			// NOTE: We are registering this watcher first in order to
			// ensure a proper test scenario. Since $digest iterates
			// over all registered watchers *in order*, if the watcher
			// below were registered first, the test expectations would pass
			// because the listener function that updates scope.nameUpper would
			// *happen* to get called first (which we cannot assume) in a single
			// execution of the forEach() iterator. Since we cannot assume this,
			// we need to test when they are in this order.
			scope.$watch(
				// Watch function.
				function(scope) { return scope.nameUpper; },
				// Listener function. Updates initial property on scope.
				// Should be picked up by $digest lifecycle.
				function(newValue, oldValue, scope) {
					if (newValue) {
						scope.initial = newValue.substring(0, 1) + '.';
					}
				}
			);

			// Watcher corresponding to name property on scope.
			scope.$watch(
				// Watch function.
				function(scope) { return scope.name; },
				// Listener function. Updates nameUpper property on scope.
				// Should be picked up by $digest lifecycle.
				function(newValue, oldValue, scope) {
					if (newValue) {
						scope.nameUpper = newValue.toUpperCase();
					}
				}
			);

			scope.$digest();
			// This means we expect the initial property to be set to 'J.' after
			// a $digest call, since (a) scope.name ('Jane') should be dirty and thus should
			// (b) call the watcher's listener function, which (c) updates scope.nameUpper,
			// which should (d) make the watcher on scope.nameUpper show up as dirty, which
			// should finally (e) call the corresponding listener function and set scope.initial to 'J.'.
			expect(scope.initial).toBe('J.');

			// This means we expect the initial property to be updated to 'B.' based on
			// changing to scope.name = 'Bob', for the same reasons listed above.
			scope.name = 'Bob';
			scope.$digest();
			expect(scope.initial).toBe('B.');
		});

		// NOTE: THIS IS NOT A TEST CONDITION FROM THE BOOK.
		it("will check on watches if dirty for at least 10 iterations", function() {
			scope.counterA = 0;
			scope.counterB = 0;

			scope.$watch(
				function(scope) { return scope.counterA; },
				function(newValue, oldValue, scope) {
					// If counterA changes and counterB < 10, inc counterB.
					if (scope.counterB < 9) {
						scope.counterB++;
					}
				}
			);

			scope.$watch(
				function(scope) { return scope.counterB; },
				function(newValue, oldValue, scope) {
					// If counterB changes and counterA < 10, inc counterA.
					if (scope.counterA < 9) {
						scope.counterA++;
					}
				}
			);

			scope.$digest();

			// This means that we expect the digest to keep iterating
			// at least 10 times (counterA & counterB are initialized to 0, hence 9)
			// without throwing an exception, since each watcher above will
			// cause the digest to be dirty and cause the other watcher to be called
			// until counterA & counterB = 9.
			expect(scope.counterA).toBe(9);
			expect(scope.counterB).toBe(9);
		});

		it("gives up on the watches after 10 iterations", function() {
			scope.counterA = 0;
			scope.counterB = 0;

			scope.$watch(
				function(scope) { return scope.counterA; },
				function(newValue, oldValue, scope) {
					// If counterA changes, inc counterB.
					scope.counterB++;
				}
			);

			scope.$watch(
				function(scope) { return scope.counterB; },
				function(newValue, oldValue, scope) {
					// If counterB changes, inc counterA.
					scope.counterA++;
				}
			);

			// This means we expect calling $digest() to throw an exception,
			// since the above watchers will cause $digest() to run infinitely,
			// since it will always be dirty (watcher 1 will cause property associated with 
			// watcher 2 to change & watcher 2 will cause property associated with watcher 1 to change.)
			// NOTE: to verify that calling $digest() will throw an exception, need to wrap it
			// and pass function reference to Jasmine expect() method, which allows us to use toThrow().
			expect((function() { scope.$digest(); })).toThrow();
		});

		it("ends the digest when the last watch is clean", function() {
			// Use lo-dash library's range() method to create an array with 100 elements in pattern:
			// [0, 1, 2, ... , 97, 98, 99].
			scope.array = _.range(100);
			// Used as a counter for how many times any of the watch functions (generated below) are called.
			var watchExecutions = 0;

			// Use lo-dash times() method to run the function below 100 times, thereby creating 100 watchers,
			// one corresponding to each element in scope.array. Each will also increment the watchExecutions 
			// counter when called.
			_.times(100, function(i) {
				scope.$watch(
					function(scope) {
						// Increment the watchExecutions counter in each watch function.
						watchExecutions++;
						// dirty condition is the corresponding element on the array changing.
						return scope.array[i];
					},
					function(newValue, oldValue, scope) {}
				);
			});

			scope.$digest();
			// This means we expect all of the watch functions to be called twice, since,
			// for the first digest call, each watcher should show up as dirty. This means the
			// last watcher in the watcher list will be dirty. The performance improvement
			// means the digest cycle should stop on the last watcher that was dirty, but since
			// all of the watchers will be dirty on initialization, this means
			// 100 watchers x 2 calls each = 200.
			expect(watchExecutions).toBe(200);

			// Update the first element in the array, which corresponds to the first watcher in
			// the watcher list.
			scope.array[0] = 420;
			scope.$digest();
			// This means we expect all of the watchers to be called again once, but the digest cycle
			// should stop once it has called the first watcher a second time.
			// The math: 200 (last digest) + 100 (digest checking for dirty watchers) + 1 (1st watcher
			// in watcher list was the only one that was dirty) = 301.
			expect(watchExecutions).toBe(301);
		});

		// NOTE: THIS IS NOT A TEST CONDITION FROM THE BOOK.
		it("sets $$lastDirtyWatch to the function that was last dirty in a digest cycle", function() {
			scope.aValue = 'abc';
			scope.bValue = 'def';

			// Need at least two watchers registered on scope to verify expected $$lastDirtyWatch.
			scope.$watch(
				function(scope) { return scope.aValue; },
				function(newValue, oldValue, scope) { }
			);
			scope.$watch(
				function(scope) { return scope.bValue; },
				function(newValue, oldValue, scope) { }
			);

			scope.$digest();
			// This means that, after a first digest call, we should expect the
			// $$lastDirtyWatch to be the last watcher added to the collection of watchers,
			// since all should show up as dirty on initialization. However, the last watcher 
			// added will be the first watcher in the collection of watchers, since new
			// watchers are added to the *beginning* of the collection of watchers.
			expect(scope.$$lastDirtyWatch).toBe(scope.$$watchers[0]);

			// Update the value associated with the first watcher, so it will be the
			// only dirty watcher.
			scope.aValue = 'ghi';
			scope.$digest();
			// This means we expect the first watcher added (last watcher in the watcher list) 
			// to be the $$lastDirtyWatch, since we only changed the scope property (aValue) 
			// associated with this watcher.
			expect(scope.$$lastDirtyWatch).toBe(scope.$$watchers[1]);
		});

		it("does not end digest so that new watches are not run", function() {
			scope.aValue = 'abc';
			scope.counter = 0;

			scope.$watch(
				function(scope) { return scope.aValue; },
				function(newValue, oldValue, scope) {
					// By adding a watcher in the listener function of another watcher,
					// this allows us to test proper behavior of digest cycle for newly
					// added items (generally and specifically with the performance improvement
					// short circuit using $$lastDirtyWatch).
					scope.$watch(
						function(scope) { return scope.aValue; },
						function(newValue, oldValue, scope) {
							scope.counter++;
						}
					);
				}
			);

			scope.$digest();
			// This means we expect the digest to have checked the watcher added
			// in the middle of the digest cycle, seen it as dirty (since all watchers
			// are considered dirty for their initial digest call), and thus called its
			// listener function.
			expect(scope.counter).toBe(1);
		});

		it("compares based on value if enabled", function() {
			scope.aValue = [1, 2, 3];
			scope.counter = 0;

			scope.$watch(
				function(scope) { return scope.aValue; },
				function(newValue, oldValue, scope) {
					scope.counter++;
				},
				// Optional 3rd param for $watch that says to dirty check based on value
				// changes instead of reference changes.
				true
			);

			scope.$digest();
			// This means we expect the listener function to have been called, since
			// it should always be called after the first $digest() call.
			expect(scope.counter).toBe(1);

			// Modify the complex object (here an Array) to verify a digest cycle will pick up 
			// that the value is dirty if comparing by value instead of reference.
			scope.aValue.push(4);
			scope.$digest();
			// This means that we expect the listener function to have been called again,
			// since our watcher is supposed to dirty check based on value change instead of
			// reference change.
			// NOTE: Dirty checking by reference is both default behavior and a less expensive check.
			expect(scope.counter).toBe(2);
		});

		// NOTE: THIS IS NOT A TEST CONDITION FROM THE BOOK.
		it("doesn't compare based on value by default", function() {
			scope.aValue = [1, 2, 3];
			scope.counter = 0;

			//Omitting optional 3rd param for watcher to demonstrate default behavior
			scope.$watch(
				function(scope) { return scope.aValue; },
				function(newValue, oldValue, scope) {
					scope.counter++;
				}/*,
				true*/
			);

			scope.$digest();
			// This means we expect the listener function to have been called, since
			// it should always be called after the first $digest() call.
			expect(scope.counter).toBe(1);

			// Modify the complex object (here an Array) to verify a digest cycle will 
			// ignore changes to values on a complex object by default.
			scope.aValue.push(4);
			scope.$digest();
			// This means that we expect the listener function to *not* have been called again,
			// since default behavior should only dirty check based on reference changes.
			// NOTE: Dirty checking by reference is both default behavior and a less expensive check.
			expect(scope.counter).toBe(1);

			// Now change the complex object referenced to verify that this *will* show up as dirty.
			scope.aValue = [5, 6, 7];
			scope.$digest();
			// This means we expect the listener function to have been called again, since
			// we actually changed the object reference on the scope and thus it should
			// show up as dirty in the digest cycle.
			expect(scope.counter).toBe(2);
		});

		it("correctly handles NaNs", function () {
			// This will evaluate to NaN
			scope.number = 0/0;
			scope.counter = 0;

			scope.$watch(
				function(scope) { return scope.number; },
				function(newValue, oldValue, scope) {
					scope.counter++;
				}
			);

			scope.$digest();
			// This means that the watcher should be considered dirty and the listener 
			// function should be called, since watchers are always considered dirty 
			// for their initial digest cycle.
			expect(scope.counter).toBe(1);

			scope.$digest();
			// This means that the watcher should *not* be considered dirty,
			// since nothing has changed. This verifies that NaNs are properly
			// compared for dirty checking in the digest cycle.
			expect(scope.counter).toBe(1);
		});


		it("has a $$phase field whose value is the current digest phase", function() {
			scope.aValue = [1,2,3];
			// Used to identify the digest cycle phase when watch functions are called.
			scope.phaseInWatchFunction = undefined;
			// Used to identify the digest cycle phase when listener functions are called.
			scope.phaseInListenerFunction = undefined;
			// Used to identify the digest cycle phase when calling $apply().
			scope.phaseInApplyFunction = undefined;

			scope.$watch(
				// The watch function.
				function(scope) {
					// Set the scope property to whatever the $$phase's value currently is.
					scope.phaseInWatchFunction = scope.$$phase;
					return scope.aValue;
				},
				// The listener function.
				function(newValue, oldValue, scope) {
					// Set the scope property to whatever the $$phase's value currently is.
					scope.phaseInListenerFunction = scope.$$phase;
				}
			);

			scope.$apply(function(scope) {
				// Set the scope property to whatever the $$phase's value is when calling $apply().
				scope.phaseInApplyFunction = scope.$$phase;
			});

			// This means we expect the digest cycle phase to be '$digest' while watch functions are
			// called.
			expect(scope.phaseInWatchFunction).toBe('$digest');
			// This means we expect the digest cycle phase to also be '$digest' while listener functions
			// are called.
			expect(scope.phaseInListenerFunction).toBe('$digest');
			// This means we expect the digest cycle phase to be '$apply' when we call $apply().
			expect(scope.phaseInApplyFunction).toBe('$apply');
		});

		// Pass in an optional "done" function to allow for an async test case. The test ends
		// whenever the done function is called, in this case after a 50 ms timeout (see below).
		it("schedules a digest in $evalAsync", function(done) {
			scope.aValue = "abc";
			scope.counter = 0;

			scope.$watch(
				// The watch function.
				function(scope) { return scope.aValue; }, 
				// The listener function. Increments the counter whenever it gets called
				// via the digest cycle.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			// Call $evalAsync(). This should schedule a digest cycle "soon", but not immediatley.
			scope.$evalAsync(function(scope) { });

			// This means we don't expect a digest cycle to begin immediately after calling
			// $evalAsync().
			expect(scope.counter).toBe(0);

			setTimeout(
				function() {
					//This means we expect a digest cycle to have been scheduled "soon" (in this case, 50ms)
					// after calling $evalAsync().
					expect(scope.counter).toBe(1);
					// Call done() to notify the test runner that the test case has ended (for async tests)
					done();
				},
				// The length of the timeout, in ms.
				50
			);
		});

		it("runs a $$postDigest function after each digest", function() {
			// Used to determine conditions under which functions added to the $$postDigest will be called.
			scope.counter = 0;

			// Add function to $$postDigest queue.
			// NOTE: Unlike $evalAsync, $$postDigest will not schedule a deferred digest cycle. Instead,
			// functions added to $$postDigest will get called after the next $digest() call, whenever it happens.
			scope.$$postDigest(function() {
				// Should increment after the next digest cycle is completed.
				scope.counter++;
			});

			// This means we don't expect the function added to $$postDigest to be called yet, since $digest() hasn't
			// been (directly or indirectly) called yet, and $$postDigest doesn't automatically schedule a digest cycle.
			expect(scope.counter).toBe(0);

			// Call $digest() so that the function added to $$postDigest will get called after the digest cycle is completed.
			scope.$digest();
			// This means we expect the function added to $$postDigest to have been called, since a digest cycle was started and
			// completed by the previous call to $digest().
			expect(scope.counter).toBe(1);

			// Call $digest() again to demonstrate that the function previously added to $$postDigest was removed after the previous
			// digest cycle finished and executed the function.
			scope.$digest();
			// This means we *don't* expect the function added to $$postDigest to have been called again, since it already executed
			// after the first digest cycle and hasn't been re-added to the $$postDigest queue.
			expect(scope.counter).toBe(1);
		});

		it("does not include $$postDigest in the digest", function() {
			// Used to check conditions when a $$postDigest will get called in relation to a digest cycle.
			scope.aValue = 'original value';

			scope.$$postDigest(function() {
				// When the function added to the $$postDigest is called, update aValue to indicate it has executed.
				scope.aValue = 'changed value';
			});

			scope.$watch(
				// The watch function.
				function(scope) {
					// Watching scope.aValue. Since the return value of a watch function
					// is used to determine the newValue param of a listener function, this
					// lets us know if/when the $$postDigest function, above, was executed.
					return scope.aValue;
				},
				// The listener function.
				function(newValue, oldValue, scope) {
					// By assigning the value of newValue to scope.watchedValue, we can
					// verify when the function added to $$postDigest was called.
					scope.watchedValue = newValue;
				}
			);

			// First call to $digest()
			scope.$digest();
			// This means we expect the listener function to have been called *before* the function added to $$postDigest. Since
			// functions added to $$postDigest should only execute *after* a digest cycle is completed, after the first call to $digest(),
			// 1) The watch function will get called and return the current value of scope.aValue (='original value')
			// 2) The listener function will get called (since this is the first time the watcher was called), passing the current value of
			//    scope.aValue (='original value') in as the 'newValue' param for the function. This will then get assigned to scope.watchedValue.
			// 3) The current digest cycle will complete and the postDigest will begin.
			// 4) The function added to $$postDigest will get called, re-assigning scope.aValue (='changed value'). However, since this happens *after*
			//    the digest cycle, it will not get picked up by the watch function (and consequently will not be used as the listener function's newValue param).
			// Thus, the watchedValue should equal the *original* value of scope.aValue (='original value').
			expect(scope.watchedValue).toBe('original value');

			// Second call to $digest()
			scope.$digest();
			// This means that the change to scope.aValue (='changed value') *should* get picked up by the watch function, since it has already happened at the end of the last
			// digest cycle, above.
			expect(scope.watchedValue).toBe('changed value');
		});

		it("catches exceptions in watch functions and continues", function () {
			scope.aValue = 'abc';
			// Used to verify that listener functions called later in a digest cycle will get executed even if an error is thrown in a watch function
			// called earlier in the digest cycle.
			scope.counter = 0;

			scope.$watch(
				// The first watch function. Throws an error.
				// NOTE: This is done in the first watch function to verify that
				// the digest cycle will continue to execute as expected even after an error is thrown.
				function(scope) { throw "Error"; },
				// The first listener function. Unused.
				function(newValue, oldValue, scope) { }
			);

			scope.$watch(
				// The second watch function.
				function(scope) { return scope.aValue; },
				// The second listener function. Increments scope.counter to verify that it was still
				// called even though an error was thrown in the first watch function.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			scope.$digest();
			// This means we expect the second listener function to still get executed even though an error
			// was thrown earlier in the digest cycle.
			expect(scope.counter).toBe(1);
		});

		it("catches exceptions in listener functions and continues", function () {
			scope.aValue = 'abc';
			// Used to verify that listener functions called later in a digest cycle will get executed even if an error is thrown earlier in the digest cycle.
			scope.counter = 0;

			scope.$watch(
				// The first watch function.
				function(scope) { return scope.aValue; },
				// The first listener function. Throws an error.
				// NOTE: This is done in the first listener function to verify that
				// the digest cycle will continue to execute as expected even after an error is thrown.
				function(newValue, oldValue, scope) { throw "Error"; }
			);

			scope.$watch(
				// The second watch function.
				function(scope) { return scope.aValue; },
				// The second listener function. Increments scope.counter to verify that it was still
				// called even though an error was thrown in the first listener function.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			scope.$digest();
			// This means we expect the second listener function to still get executed even though an error
			// was thrown earlier in the digest cycle.
			expect(scope.counter).toBe(1);
		});

		it("catches exceptions in $$postDigest", function() {
			var didRun = false;

			// The first function added to the postDigest queue. Throws an error. This allows us to verfy that
			// subsequent functions added to the postDigest queue will still be executed even though an error was thrown earlier.
			scope.$$postDigest(function() {
				throw "Error";
			});

			// The second function added to the postDigest queue. Toggles the didRun var to true to verify that the function still
			// executed even though an error was thrown earlier.
			scope.$$postDigest(function() {
				didRun = true;
			});

			// Call $digest() to cause the functions in the postDigest queue to execute.
			scope.$digest();
			// This means we expect the second function added to the postDigest queue to have executed even though an error was thrown
			// earlier while looping through the queue.
			expect(didRun).toBe(true);
		});

		it("allows destroying a $watch with a removal function", function() {
			scope.aValue = 'abc';
			scope.counter = 0;

			// The scope's $watch() function should return a function that can be called
			// to remove/destroy the watcher from the scope.
			var destroyWatch = scope.$watch(
				// The watch function.
				function(scope) { return scope.aValue; },
				// The listener function. Increments counter to verify if/when the listener function
				// is called.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			scope.$digest();
			// Since all watchers are assumed dirty on initial call (and thus call the listener
			// function), we expect scope.counter to have been incremented to 1.
			expect(scope.counter).toBe(1);

			// Update the value watched by the watcher to verify that initiating a digest cycle
			// via $digest() will call the listener function, since the watcher hasn't been removed/destroyed.
			scope.aValue = 'def';
			scope.$digest();
			// Since the watcher hasn't yet been removed, the listener function should be called again,
			// thus incrementing scope.counter again (to 2).
			expect(scope.counter).toBe(2);

			// Update the value watched by the watcher again.
			scope.aValue = 'ghi';
			// Call the function returned by the call to scope.$watch(), above. This should remove/destroy
			// the watcher. We're calling this *before* the $digest() call to demonstrate that the listener
			// function should no longer get called.
			destroyWatch();
			scope.$digest();
			// This means we don't expect the listener function to have been called, since we've removed
			// the watcher from the scope by calling the function returned by our call to scope.$watch().
			expect(scope.counter).toBe(2);
		});

		it("allows destroying a $watch during digest", function() {
			scope.aValue = 'abc';

			// Used to accumulate calls to specific watcher functions and verify
			// that they are correctly destroyed when the removal/destroying function is called
			// in the middle of a digest cycle.
			var watchCalls = [];

			scope.$watch(
				// Watcher function.
				function(scope) {
					// Every time this watcher is called from a digest cycle,
					// add 'first' to the list.
					watchCalls.push('first');
					return scope.aValue;
				}
			);

			// Store a reference to the removal/destroying function. Used below.
			var destroyWatch = scope.$watch(
				// Watcher function.
				function(scope) {
					// Every time this watcher is called from a digest cycle,
					// add 'second' to the list. This should only happen once,
					// since we are immediately removing/destroying this watcher
					// the first time it's called.
					watchCalls.push('second');
					// This destroys/removes the watcher from the watcher list,
					// meaning it should no longer get executed in a digest cycle.
					destroyWatch();
				}
			);

			scope.$watch(
				// Watcher function.
				function(scope) {
					// Every time this watcher is called from a digest cycle,
					// add 'third' to the list.
					watchCalls.push('third');
					return scope.aValue;
				}
			);

			scope.$digest();

			// This means we expect the first and third watchers to get executed twice,
			// but the second to only get executed once, since it immediately removes/destroys
			// itself from the watchers the first time it is executed.
			expect(watchCalls).toEqual(['first','second','third','first','third']);
		});

		it("allows a $watch to destroy another during digest", function() {
			scope.aValue = 'abc';
			// Used to increment calls to a watcher, below.
			scope.counter = 0;

			scope.$watch(
				// Watcher function. Unused.
				function(scope) {
					return scope.aValue;
				},
				// Listener function. Destroys/Removes a watcher other than itself, below.
				function(newValue, oldValue, scope) {
					destroyWatch();
				}
			);

			// Save a reference to the removal/destroying function. Used in watcher, above,
			// to remove watcher from the digest.
			var destroyWatch = scope.$watch(
				function(scope) { },
				function(newValue, oldValue, scope) { }
			);

			scope.$watch(
				function(scope) { return scope.aValue; },
				// Listener function.
				function(newValue, oldValue, scope) {
					// Increment counter to verify that removing another watcher
					// located earlier in the watcher list doesn't impact calls to 
					// this watcher by shifting indices.
					scope.counter++;
				}
			);

			scope.$digest();
			// This means we expect the third watcher to get called from an initial digest
			// cycle, verifying that destroying/removing a different watcher during the cycle
			// doesn't break expected functionality.
			expect(scope.counter).toBe(1);
		});

		// NOTE: Although this test is from the book, it doesn't actually test the right
		// condition, since the counter will not execute regardless once the watcher's been
		// removed. What happens instead is an error is thrown because the watcher doesn't
		// exist. However, since the digest cycle handles errors, this test will pass without
		// the appropriate null check on the watcher in $$digestOnce(). Keeping test for
		// consistency with the book. (CJP).
		it("allows a $watch to destroy several $watches during digest", function() {
			scope.aValue = 'abc';
			scope.counter = 0;

			var destroyWatch1 = scope.$watch(
				function(scope) {
					destroyWatch1();
					destroyWatch2();
				}
			);

			var destroyWatch2 = scope.$watch(
				function(scope) { return scope.aValue; },
				function(newValue, oldValue, scope) {
					scope.counter++;
				}
			);

			scope.$digest();
			expect(scope.counter).toBe(0);
		});

		it("executes $eval'ed functions and returns a result", function() {
			scope.aValue = 42;

			// This will execute the function passed in to $eval() and return its return value.
			var result = scope.$eval(function(scope) {
				return scope.aValue;
			});

			// This means we expect a call to $eval() with a function that returns scope.aValue (= 42)
			// to return the function's value, demonstrating how $eval() should work.
			expect(result).toBe(42);
		});

		it("passes the second $eval argument straight through", function() {
			scope.aValue = 42;

			// This will execute the function passed in to $eval(), upassing $eval's 2nd param
			// as a param to said function, and return its return value.
			var result = scope.$eval(function(scope, arg) {
				return scope.aValue + arg;
			}, 2);

			// This means we expect a call to $eval() with an optional 2nd param to pass it on
			// to the function being $eval'ed.
			expect(result).toBe(44);
		});

		it("executes $apply'ed function and starts the digest", function() {
			scope.aValue = 'someValue';
			scope.counter = 0;

			scope.$watch(
				function(scope) {
					return scope.aValue;
				},
				function(newValue, oldValue, scope) {
					scope.counter++;
				}
			);

			// We need to run the digest cycle once to have a valid test case, since
			// all watchers will be considered dirty on their initial run.
			scope.$digest();
			expect(scope.counter).toBe(1);

			// Calling $apply() should a) $eval() (execute in Angular context) the method below
			// and b) call $digest(), thereby kicking off a digest cycle.
			scope.$apply(function(scope) {
				// This should make the watcher above show up as dirty.
				scope.aValue = 'someOtherValue';
			});
			// This means we expect the listener function on the watcher above to have been called,
			// since calling $apply() should both execute the function passed into it and kick off
			// a digest cycle.
			expect(scope.counter).toBe(2);
		});

		it("executes $evalAsync'ed function later in the same cycle", function() {
			scope.aValue = [1, 2, 3];
			// Use this to verify that $evalAsync() calls the function passed in
			// when a digest cycle occurs.
			scope.asyncEvaluated = false;
			// Use this to verify that the function passed in does *not* get called
			// synchronously (in this case, when the watcher's listener function is 
			// called during a digest cycle).
			scope.asyncEvaluatedImmediately = false;

			scope.$watch(
				function(scope) { return scope.aValue; },
				// Listener function.
				function(newValue, oldValue, scope) {
					// Add a function that should be evaluated at the end of a dirty digest cycle
					scope.$evalAsync(function(scope) {
						// Once the function is executed (asynchronously), update property to verify
						// it was executed after the digest cycle.
						scope.asyncEvaluated = true;
					});
					// Since this is in the watcher's listener function but outside of the async function,
					// it will be executed synchronously when a watcher is dirty (which means the property
					// should be false).
					scope.asyncEvaluatedImmediately = scope.asyncEvaluated;
				}
			);

			scope.$digest();
			// This means we expect the async function to have executed, since all watchers are dirty on
			// their initial run, which means the watcher's listener function should have been called,
			// adding the function to the async queue which should get executed at the end of the digest
			// cycle (since it was a dirty digest cycle).
			expect(scope.asyncEvaluated).toBe(true);
			// This means we expect the listener function to execute in its entirety before the async
			// evaluated function is called. Since 1) "scope.asyncEvalutedImmediately = scope.asyncEvaluated"
			// will be executed *before* 2) "scope.asyncEvaluated = true", scope.asyncEvaluated will still be
			// false when (1) is executed. Only later in the digest cycle will (2) be executed.
			expect(scope.asyncEvaluatedImmediately).toBe(false);
		});

		it("executes $evalAsync'ed functions added by watch functions", function() {
			scope.aValue = [1,2,3];
			// Used as a flag in the $evalAsync function to verify it was added by the
			// watch function and called during the digest cycle.
			scope.asyncEvaluated = false;

			scope.$watch(
				// The watch function. Should be called by calling $digest(), below.
				function (scope) {
					// If this flag is still false, that means the $evalAsync function below
					// hasn't been called yet. If it hasn't, add the function to the
					// asyncQueue. This ensures that the function will only be added once.
					if (!scope.asyncEvaluated) {
						// The evalAsync function.
						// NOTE: Bad practice. Watch functions should *NEVER* have side effects
						// (i.e. effects on application/module state) unless necessary. The purpose
						// of a watch function is to *CHECK* state, not *CHANGE* state.
						scope.$evalAsync(function(scope) {
							// Switch the asycEvaluated flag to true. If it's true
							// on the scope, this means the function was called by
							// the digest cycle started by calling $digest(), below.
							// It also ensures that it wil only be added once by the
							// watch function (see above).
							scope.asyncEvaluated = true;
						});
					}
					return scope.aValue;
				},
				// The listener function. Unused.
				function(newValue, oldValue, scope) { }
			);

			scope.$digest();

			// This means we expect the evalAsync function to have been called by calling
			// $digest(), since it will set asyncEvaluted = true.
			expect(scope.asyncEvaluated).toBe(true);
		});

		it("executes $evalAsync'ed functions even when not dirty", function() {
			scope.aValue = [1,2,3];
			// This is used to verify that the evalAsync function will be
			// called even if the digest isn't dirty.
			scope.asyncEvaluatedTimes = 0;

			scope.$watch(
				// The watch function. Conditionally adds an evalAsync function.
				function(scope) {
					// If asyncEvalutedTimes is under two, that means the evalAsync
					// function has been called 0 or 1 times. On these conditions, 
					// re-add it to the asyncQueue. We need to add it twice, since
					// a watcher is always considered dirty on its initial run, so
					// it's only on the 2nd pass of the digest cycle that we can
					// truly verify that an evalAsync function will be called even
					// when the digest cycle or a particular watcher is not dirty.
					if (scope.asyncEvaluatedTimes < 2) {
						// The evalAsync function. increments the asyncEvalutedTimes
						// each time it's called.
						scope.$evalAsync(function(scope) {
							// Increment the counter. Should only happen twice.
							scope.asyncEvaluatedTimes++;
						});
					}

					return scope.aValue;
				},
				// The listener function. Unused.
				function(newValue, oldValue, scope) { }
			);

			scope.$digest();

			// This means we expect the evalAsync function to be called twice
			// when we start a digest cycle by calling $digest, above. The first
			// time is because every watcher is presumed potentially dirty until shown clean;
			// the second time is because the digest cycle should check for *both* dirty watchers
			// *and* functions in the asyncQueue. If the asyncQueue's length is > 0, the digest cycle
			// should continue.
			expect(scope.asyncEvaluatedTimes).toBe(2);
		});

		it("eventually halts $evalAsyncs added by watchers", function() {
			scope.aValue = [1,2,3];

			scope.$watch(
				// The watch function. Always adds an evalAsync function to the asyncQueue.
				function(scope) {
					// The evalAsync function. Since the watch function adds it to the 
					// asyncQueue *unconditionally*, this will lead to an infinite loop of
					// the digest cycle, since the digest will keep looping if anything is in
					// the asyncQueue, and the watch function will keep adding the evalAsync
					// function to the queue each time it's called in the digest loop.
					scope.$evalAsync(function(scope) { });
					return scope.aValue;
				},
				// The listener function. Unused.
				function(newValue, oldValue, scope) { }
			);

			// This means we expect calling $digest() to throw an error, since the above code
			// will make it exceed the max number of iterations it's configured to cycle (default = 10).
			// This mirrors the expected behavior of an unstable watcher.
			expect(function() { scope.$digest(); }).toThrow();
		});

		it("catches exceptions in $evalAsync", function(done) {
			scope.aValue = 'abc';
			scope.counter = 0;

			scope.$watch(
				function(scope) { return scope.aValue; },
				// The listener function. Increments scope.counter to verify that this will still be executed even
				// though the function added to the async queue throws an error.
				function(newValue, oldValue, scope) { scope.counter++; }
			);

			// Add a function to the async queue that throws an error.
			scope.$evalAsync(function(scope) { throw "Error"; });

			setTimeout(function() {
				// This means we expect the listener function to eventually be called, even though an error was thrown in the digest cycle
				// by the evalAsync function.
				expect(scope.counter).toBe(1); 
				// Notifies the test runner that the async test is completed.
				done();
			}, 50);
		});
	});

	describe("Inheritance", function() {
		
		it("inherits the parent's properties", function() {
			// Create a parent scope.
			var parent = new Scope();
			// Add a property with a value assigned to it to the parent scope.
			parent.aValue = [1,2,3];

			// Create a child scope based on the parent scope using its $new() method.
			var child = parent.$new();

			// This means we expect a child created from a parent to inherit the parent's
			// properties with the values assigned to those properties.
			expect(child.aValue).toEqual([1,2,3]);
		});

		it("does not cause a parent to inherit its properties", function() {
			// Create a parent scope.
			var parent = new Scope();

			// Create a child scope based on the parent scope using its $new() method.
			var child = parent.$new();
			// Add a property with a value assigned to it to the child scope.
			child.aValue = [1,2,3];

			// This means we do *not* expect a parent scope to acquire properties created on
			// the child scope, hence 'aValue' should still be undefined on the parent scope,
			// since it was never defined on that scope, only on the child.
			expect(parent.aValue).toBeUndefined();
		});

		it("inherits the parent's properties whenever they are defined", function() {
			// Create a parent scope.
			var parent = new Scope();
			// Create a child scope based on the parent scope using its $new() method.
			var child = parent.$new();

			// Add a property with a value assigned to it to the parent scope *after*
			// the child scope has been createdx.
			parent.aValue = [1,2,3];

			// This means we expect the child scope to *still* have the property added
			// to the parent scope, even though that property was added *after* the child was created.
			expect(child.aValue).toEqual([1,2,3]);
		});

		it("can manipulate a parent's scope property", function() {
			// Create a parent scope.
			var parent = new Scope();
			// Create a child scope based on the parent.
			var child = parent.$new();
			// Add a property to the parent scope. This should be shared with the child scope.
			parent.aValue = [1,2,3];

			// Update the property using the *child* scope. Even though the parent will not
			// pick up new properties added to the child scope, nor will it pick up
			// when a property is assigned an entirely new value, it *should* pick up
			// when a complex object assigned to a property is *modified* (in this case, an Array).
			// This is because parent.someProperty and child.someProperty both point to *the same*
			// object reference.
			child.aValue.push(4);

			// This means we (unsurprisingly) expect the child's aValue property to have updated,
			// since we explicitly changed it above by pushing another element onto the Array.
			expect(child.aValue).toEqual([1,2,3,4]);
			// This means we expect the *parent's* aValue property to have updated when changes
			// are made from the child's aValue property, since the object reference is the
			// same for both.
			expect(parent.aValue).toEqual([1,2,3,4]);
		});

		it("can watch a property in the parent", function() {
			// Create a parent scope.
			var parent = new Scope();
			// Create a child scope.
			var child = parent.$new();
			// Add a property to the parent scope that is shared with the child scope.
			parent.aValue = [1,2,3];
			// Create a counter to verify the number of times a watcher's listener function
			// is called.
			child.counter = 0;

			child.$watch(
				// Watcher function. Used to verify changes made to a property on the
				// parent scope will be picked up by a watcher of that property on the child scope.
				function(scope) { return scope.aValue; },
				// Listener function.
				function(newValue, oldValue, scope) {
					// Increment the counter to verify that a change was picked up by the child's property watcher.
					scope.counter++;
				},
				// This sets the watcher to check by *value* instead of by reference. Since we are updating
				// elements in an Array property in this test case (and not assigning an entirely new value to
				// the watched property), we need to set this to true.
				true
			);

			child.$digest();
			// This means we expect the child watcher to have called the listener function, since these are always
			// called on initial digest of a new watcher. This should happen even though the property was added to
			// the parent scope.
			expect(child.counter).toBe(1);

			// Update parent.aValue to verify that the change will be picked up by the child watcher on the next
			// digest cycle.
			parent.aValue.push(4);
			// Kick off another digest cycle on the child.
			child.$digest();

			// This means we expect the child watcher to have picked up the change and thus called the listener funciton.
			expect(child.counter).toBe(2);
		});

		it("can be nested at any depth", function() {
			// Top parent scope.
			var a 	= new Scope();

			// Descendants of a.
			var aa = a.$new();
			// Children of aa (Still descendants of a)
			var aaa = aa.$new();
			var aab = aa.$new();

			var ab = a.$new();
			// Child of ab (Still descendants of a but not of aa)
			var aba = ab.$new();

			// Add a property with a value assigned to it to the root parent scope. Used
			// to demonstrate that *all* descendants should inherit this property with its value.
			a.value = 1;

			// This means we expect all descendants of a to have inherited the property/value added to a.
			expect(aa.value).toBe(1);
			expect(aaa.value).toBe(1);
			expect(aab.value).toBe(1);
			expect(ab.value).toBe(1);
			expect(aba.value).toBe(1);

			// Add a property with a value assigned to it to one of the root parent scope's children.
			// Used to verify that *only* its descendants (and not anscestors or siblings & siblings' descendants)
			// should inherit the property/value added to it.
			ab.anotherValue = 2;

			// This means we expect descendants of ab to have inherited the property/value added to ab.
			expect(aba.anotherValue).toBe(2);
			// This means, on the other hand, that we *do not* expect *siblings* of ab or their descendants
			// to have inherited the property/value added to ab.
			expect(aa.anotherValue).toBeUndefined();
			expect(aaa.anotherValue).toBeUndefined();
		});

		it("shadows a parent's property with the same name", function() { 
			// Create parent scope.
			var parent = new Scope();
			// Create child scope from parent.
			var child = parent.$new();

			// Add a property to the parent scope.
			parent.name = 'Joe';

			// Assign a new value to the property on the child scope. This is used to demonstrate that this
			// updates the child's property but not the parent's.
			// NOTE: In fact, this is creating a *new* property on the child scope with the same name as the
			// property on the parent scope. Before this, the child object instance didn't have a property ("name"),
			// at least not directly. Instead, it used the property found somewhere up its prototypal inheritance chain.
			// Adding a property to a child with the same name as one found on its parent (along with its effects) is
			// called "shadowing".
			child.name = 'Jill';

			// This means we (unsurprisingly) expect the child scope's property to have updated, since we explicitly
			// changed it above.
			expect(child.name).toBe('Jill');

			// This means we expect the *parent scope's* property to *not* have updated, since assigning new values to
			// a property on a child scope actually creates an entirely new property, one not shared by the parent.
			expect(parent.name).toBe('Joe');

			// NOTE: THIS EXPECTATION IS NOT FROM THE BOOK BUT DEMONSTRATES ANOTHER IMPORTANT SIDE OF PROPERTY SHADOWING
			// ON CHILD SCOPES! (CJP)

			// Now assign a new value to the parent scope's property. This is used to demonstrate the other side
			// of "shadowing" a property by assigning it a new value on the child: it actually creates an entirely
			// new property on the child scope, so the property on the child will no longer update when changes are made
			// to the parent.
			parent.name = 'Jack';

			// This means we expect the child scope's property to no longer change when we update it on the parent scope,
			// since this property has been "shadowed" on the child once it was reassigned.
			expect(child.name).toBe('Jill');
		});

		it("does not shadow members of parent scope's attributes", function() { 
			// Create parent scope.
			var parent = new Scope();
			// Create child scope from parent.
			var child = parent.$new();

			// Add a property to store a complex object on the parent scope, which the child should inherit.
			parent.user = {name: 'Joe'};

			// Use the child scope to update a property on the *complex object* referenced by the parent & child scope's
			// property. Used to demonstrate that, although changing the value of a *property* on the child scope will
			// shadow its parent's property, wrapping a property in an object should not.
			child.user.name = 'Jill';

			// This means we (unsurprisingly) expect the property on the complex object assigned to the child's property 
			// to have updated, since we changed its value above.
			expect(child.user.name).toBe('Jill');

			// This means we expect the property on the complex object assigned to the *parent's* property to have updated,
			// demonstrating that shadowing doesn't occur when properties are wrapped by complex objects.
			// NOTE: This demonstrates a common strategy for allowing child scopes to update properties associated on their
			// ancestors, up to their root scope.
			// NOTE: This strategy is also known as the "dot rule".
			expect(parent.user.name).toBe('Jill');
		});

		it("does not digest its parent(s)", function() { 
			var parent = new Scope();
			var child = parent.$new();
			
			parent.aValue = 'abc';
			
			// Add a watcher to the *parent scope.*
			parent.$watch(
				// Watch function. Unused.
				function(scope) { return scope.aValue; }, 
				// Listener function.
				function(newValue, oldValue, scope) {
					// Set new property/value on the parent scope. Used to demonstrate
					// that this watcher shouldn't get called at all when initiating a digest
					// cycle from the *child scope*.
					scope.aValueWas = newValue;
				}
			);
			
			// Kick off a digest on the child scope, which should only digest/check watchers
			// added to the child or any of its descendants (in this case, none), not its parents,
			// siblings, or siblings' descendants.
			child.$digest();
			
			// This means that we don't expect scope.aValueWas to have been updated (even though the
			// child inherits this property/value from its parent), since the *parent scope's* watcher should never
			// get called when initiating a digest on its *child*.
			expect(child.aValueWas).toBeUndefined();
		});

		it("keeps a record of its children", function() { 
			var parent = new Scope();
			// Create children scopes of parent.
			var child1 = parent.$new();
			var child2 = parent.$new();
			// Create child scope of a child scope (and thus descendant of parent scope)
			var child2_1 = child2.$new();

			// These mean we expect the $$children property on a parent to keep track of all
			// and only its own children, not grandchildren/other descendants, nor children of
			// siblings, etc.
			expect(parent.$$children.length).toBe(2);
			expect(parent.$$children[0]).toBe(child1);
			expect(parent.$$children[1]).toBe(child2);
			expect(child1.$$children.length).toBe(0);
			expect(child2.$$children.length).toBe(1);
			expect(child2.$$children[0]).toBe(child2_1);
		});

		it("digests its children", function() { 
			var parent = new Scope();
			var child = parent.$new();
			
			parent.aValue = 'abc';
			
			// Add a watcher to the *child scope*. Used to demonstrate that digest cycles
			// initiated on a parent scope digest its children (and all other descendants)
			child.$watch(
				function(scope) { return scope.aValue; }, 
				function(newValue, oldValue, scope) {
					// Add a property on the *child scope*. This should get updated even
					// though the digest cycle is getting initiated on the parent scope.
					scope.aValueWas = newValue;
				}
			);
			
			// Kick off a digest cycle on the parent scope. This should check/digest all watchers
			// on the parent *and* all of its descendants (= child scope)
			parent.$digest();
			
			// This means we expect the child scope's listener function to have executed, since
			// digest cycles on a parent should check/digest watchers on its descendants.
			expect(child.aValueWas).toBe('abc');
		});

		it("digests from root on $apply", function() { 
			// Create parent and descendant scopes.
			var parent = new Scope();
			var child = parent.$new();
			var child2 = child.$new();
			
			parent.aValue = 'abc';
			parent.counter = 0;
			
			// Add watcher to the parent/root scope.
			parent.$watch(
				// The watch function.
				function(scope) { return scope.aValue; }, 
				// The listener function. Used to verify that calling $apply() on a descendant
				// scope will kick off a digest cycle on the root (parent) scope.
				function(newValue, oldValue, scope) {
					// Increment the counter to verify that the listener function in the root 
					// scope's watchers was executed when $apply() is called on one of its descendants.
					scope.counter++;
				}
			);
			
			// Call $apply() on the root scope's descendant (child's child).
			child2.$apply(function(scope) { });
			
			// This means we expect the root scope's listener function to get executed when $apply()
			// is called on its descendant.
			expect(parent.counter).toBe(1);
		});

		it("schedules a digest from root on $evalAsync", function(done) { 
			// Create parent and descendant scopes.
			var parent = new Scope();
			var child = parent.$new();
			var child2 = child.$new();
			
			parent.aValue = 'abc';
			parent.counter = 0;
			
			// Add watcher to the parent/root scope.
			parent.$watch(
				// The watch function.
				function(scope) { return scope.aValue; },
				// The listener function. Used to verify that calling $evalAsync() on a descendant
				// scope will kick off a digest cycle on the root (parent) scope. 
				function(newValue, oldValue, scope) {
					// Increment the counter to verify that the listener function in the root 
					// scope's watchers was executed when $evalAsync() is called on one of its descendants.
					scope.counter++;
				}
			);

			// Call $evalAsync() on the root scope's descendant (child's child).
			child2.$evalAsync(function(scope) { });
			
			setTimeout(function() { 
				//This means we expect a digest cycle to have been scheduled "soon" (in this case, within 50ms)
				// on *the root scope* after calling $evalAsync() on one of its descendants.
				expect(parent.counter).toBe(1);
				// Call done() to notify the test runner that the test case has ended (for async tests)
				done();
			}, 50);
		});

		it("does not have access to parent attributes when isolated", function() { 
			// Create parent scope.
			var parent = new Scope();
			// Create isolate scope from parent scope (by passing 'true' into the $new constructor method).
			// An isolate scope is not created using prototypal inheritance.
			var child = parent.$new(true);
			
			// Add a property to the *parent* scope. Used to demonstrate that isolate scopes created from a
			// parent scope will not inherit properties from the parent (or any anscestor) scope.
			parent.aValue = 'abc';
			
			// This means we expect isolate scopes to not inherit properties from scope used to create them,
			// since they are not created using prototypal inheritance.
			expect(child.aValue).toBeUndefined();
		});

		it("cannot watch parent attributes when isolated", function() { 
			// Create parent scope.
			var parent = new Scope();
			// Create isolate scope from parent scope (by passing 'true' into the $new constructor method).
			// An isolate scope is not created using prototypal inheritance.
			var child = parent.$new(true);
			
			// Add a property to the *parent* scope. Used to demonstrate that, since isolate scopes created from a
			// parent scope will not inherit properties from the parent (or any anscestor) scope, they also
			// cannot watch properties found on the parent scope.
			parent.aValue = 'abc';
			
			// Add watcher to the child *isolate* scope that watches the property added to the *parent* scope.
			child.$watch(
				// Watch function. Used to demonstrate that the child isolate scope will not have
				// "aValue" as a property.
				function(scope) { return scope.aValue; }, 
				// Listener function. Used to demonstrate that watcher won't show up as dirty, since the child
				// isolate scope doesn't have "aValue" and therefore the listener will not get executed.
				function(newValue, oldValue, scope) {
					scope.aValueWas = newValue;
				}
			);

			child.$digest();
			
			// This means we expect the listener function to not have been executed, since the child isolate
			// scope cannot watch properties added to the parent scope, since it was not created using prototypal inheritance.
			expect(child.aValueWas).toBeUndefined();
		});

		it("digests its isolated children", function() { 
			// Create parent scope and isolate child scope.
			var parent = new Scope();
			var child = parent.$new(true);
			
			// Assign a property to the isolate scope to watch.
			child.aValue = 'abc';
			
			// Add a watcher to the child scope. Used to verify that ancestors of isolate scopes will still digest
			// said isolate scope.
			child.$watch(
				// Watch function.
				function(scope) { return scope.aValue; },
				// Listener function.
				function(newValue, oldValue, scope) {
					// Add new property to isolate scope to verify that watcher on isolate scope was digested
					// when initiating a digest cycle on its ancestor.
					scope.aValueWas = newValue;
				}
			);
			
			// Initiate a digest cycle on the ancestor of the isolate scope.
			parent.$digest();
			
			// This means we expect isolate scope watchers to be digested when a digest cycle is initiated on
			// one of its ancestors.
			expect(child.aValueWas).toBe('abc');
		});

		it("digests from root on $apply when isolated", function() { 
			// Create parent, isolate child scope, and child of isolate scope.
			var parent = new Scope();
			var child = parent.$new(true);
			var child2 = child.$new();
			
			// Add property and counter to parent scope to verify that calling $apply()
			// on descendant isolate scope will still initiate a digest cycle on its root ancestor
			// (in this case, "parent").
			parent.aValue = 'abc';
			parent.counter = 0;
			
			// Add watcher to parent scope.
			parent.$watch(
				function(scope) { return scope.aValue; },
				function(newValue, oldValue, scope) { 
					// Used to verify that watcher was digested on parent scope when calling $apply()
					// on descendant isolate scope.
					scope.counter++;
				}
			);
			
			// Kick off a digest cycle on the root ancestor by calling $apply() on the child of an isolate scope.
			child2.$apply(function(scope) { }); 
			
			// This means we expect the parent scope's watcher to have been digested when we call $apply() on one of
			// its descendants, even if those are or descend from an isolate child scope.
			expect(parent.counter).toBe(1);
		});
		
		it("schedules a digest from root on $evalAsync when isolated", function(done) { 
			// Create parent, isolate child scope, and child of isolate scope.
			var parent = new Scope();
			var child = parent.$new(true);
			var child2 = child.$new();
			
			// Add property and counter to parent scope to verify that calling $evalAsync()
			// on descendant isolate scope will still initiate a digest cycle on its root ancestor
			// (in this case, "parent").
			parent.aValue = 'abc';
			parent.counter = 0;
			
			// Add watcher to parent scope.
			parent.$watch(
				function(scope) { return scope.aValue; }, 
				function(newValue, oldValue, scope) {
					// Used to verify that watcher was digested on parent scope when calling $evalAsync()
					// on descendant isolate scope.
					scope.counter++;
				}
			);
			
			// Kick off a deferred digest cycle on the root ancestor by calling $evalAsync() on the child of an isolate scope.
			child2.$evalAsync(function(scope) { });
			
			setTimeout(function() { 
				// This means we expect the parent scope's watcher to have been digested when we call $evalAsync() on one of
				// its descendants, even if those are or descend from an isolate child scope.
				expect(parent.counter).toBe(1);
				// Call done() to notify the test runner that the test case has ended (for async tests)
				done();
			}, 50);
		});

		it("executes $evalAsync functions on isolated scopes", function(done) { 
			// Create parent and isolate child scope.
			var parent = new Scope();
			var child = parent.$new(true);
			
			// Issue an $evalAsync() on the child isolate scope to verify that it works
			// properly on isolate scopes.
			child.$evalAsync(function(scope) { 
				// Use property to verify that function added to the evalAsync queue was
				// actually executed.
				scope.didEvalAsync = true;
			});

			setTimeout(function() { 
				// This means we expect the function added to the evalAsync queue to 
				// (eventually) execute on the isolate scope when calling $evalAsync().
				expect(child.didEvalAsync).toBe(true);
				// Call done() to notify the test runner that the test case has ended (for async tests)
				done();
			}, 50);
		});

		it("executes $$postDigest functions on isolated scopes", function() { 
			// Create parent and isolate child scope.
			var parent = new Scope();
			var child = parent.$new(true);
			
			// Add function to the $$postDigest queue to verify that it works properly
			// on isolate scopes.
			child.$$postDigest(function() { 
				// Use property to verify that function added to queue actually executes.
				child.didPostDigest = true;
			});

			// Kick off a digest cycle on the parent scope, which should recurse down to
			// all of its descendants, including the isolate scope.
			parent.$digest();
			
			// This means we expect the function added to the isolate scope's $$postDigest
			// queue to have executed when a digest cycle was initiated from its anscestor.
			expect(child.didPostDigest).toBe(true); 
		});

		it("is no longer digested when $destroy has been called", function() { 
			// Create parent and child scope.
			var parent = new Scope();
			var child = parent.$new();
			
			// Add property to watch on the child scope.
			child.aValue = [1, 2, 3];
			// Add counter to verify that watchers are no longer called once the child
			// has been destroyed.
			child.counter = 0;
			
			child.$watch(
				function(scope) { return scope.aValue; }, 
				function(newValue, oldValue, scope) {
					scope.counter++;
				},
				true
			);
			
			// First digest cycle. Should digest all descendants.
			parent.$digest();
			
			// This means we expect the child scope's watcher to have been digested, since
			// it hasn't been destroyed yet and since all watchers are assumed dirty on initialization.
			expect(child.counter).toBe(1);
			
			// Update the watched property.
			child.aValue.push(4);
			
			// Second digest.
			parent.$digest();
			
			// This means we expect the child scope's watcher to have been digested, providing
			// a non-initialization verification.
			expect(child.counter).toBe(2);
			
			// Now destroy the child scope. This should remove it from the parent scope's list of children,
			// which means, among other things, its watchers should no longer get digested from the parent.
			child.$destroy();
			
			// Update the watched property again. Used to verify that the watcher will no longer get digested.
			child.aValue.push(5);
			
			// Third digest, post-destruction of the child scope.
			parent.$digest();
			
			// This means we *don't* expect the child scope's watcher to have been digested, since it
			// was destroyed.
			expect(child.counter).toBe(2);
		});
	});
});