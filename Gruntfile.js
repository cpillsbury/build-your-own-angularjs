module.exports = function(grunt) {

	grunt.initConfig({
		// grunt config for jshint
		jshint: {
			// Runs jshint "linting" against all *.js files in both src and test dirs.
			all: ['src/**/*.js', 'test/**/*.js'],
			options: {
				globals: {
					// global variable defined in Lo-Dash
					_: false,
					// global variable defined in jQuery.
					$: false,
					// global variables defined in Jasmine (used for testing).
					jasmine: false,
					describe: false,
					it: false,
					expect: false,
					beforeEach: false
				},
				// enables browser & devel JSHint environments.
				browser: true,
				devel: true
			}
		},
		// grunt config for testem test runner.
		testem: {
			// declares Testem task.
			unit: {
				options: {
					// Use jasmine test framework for Testem.
					framework: 'jasmine2',
					// launches the PhantomJS headless Webkit browser for running tests in dev.
					launch_in_dev: ['PhantomJS'],
					// Run jshint before running the Testem unit test task.
					before_tests: 'grunt jshint',
					serve_files: [
						// library dependency files to include
						'node_modules/lodash/lodash.js',
						'node_modules/jquery/dist/jquery.js',
						// test & src JS files
						'src/**/*.js', 
						'test/**/*.js'
					],
					// Watch for changes to any *.js files in src or test dirs and re-run tests if changed.
					watch_files: ['src/**/*.js', 'test/**/*.js']
				}
			}
		}
	});

	// Above provides the config. This actually loads the npm tasks.
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-testem');

	// Make running the Testem unit task the default grunt task.
	// For this project, simply typing "grunt" will now kick off our unit tests.
	grunt.registerTask('default', ['testem:run:unit']);

};
